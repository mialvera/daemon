#include "csapp.h"
#include "fft.h"
#include "commands.h"

const size_t S_DATA = sizeof(complex)*N;

enum Estado {EXIT = 0, WAITING_COMMAND, SENDING_DATA, WAITING_DATA, DISPLAY_DATA, POLICY} estado; //Estados para la FSM

complex data[N];

static void llenar_datos()
{
	int k;

	for(k=0; k<N; k++) {
		data[k].Re = 0.125*cos(2*PI*k/(double)N);
		data[k].Im = 0.125*sin(2*PI*k/(double)N);
	}
}

/* Print a vector of complexes as ordered pairs. */
static void print_vector(const char *title, complex *x, int n)
{
	int i;
	printf("%s (dim=%d):", title, n);
	for(i=0; i<n; i++ ) printf(" %5.2f,%5.2f ", x[i].Re,x[i].Im);
	putchar('\n');
	return;
}

int main(int argc, char **argv)
{
	int clientfd;
	char *port;
	char *host, buf[MAXLINE];
	rio_t rio;
	estado = WAITING_COMMAND;
	size_t n;

	if (argc != 3) {
		fprintf(stderr, "usage: %s <host> <port>\n", argv[0]);
		exit(0);
	}
	host = argv[1];
	port = argv[2];

	llenar_datos();

	clientfd = Open_clientfd(host, port);
	Rio_readinitb(&rio, clientfd);

	while(estado) {
		switch(estado)
		{
			case WAITING_COMMAND:
				Fgets(buf, MAXLINE, stdin);
				Rio_writen(clientfd, buf, strlen(buf));
				n = Rio_readlineb(&rio, buf, MAXLINE);
				Fputs(buf, stdout);

				if(strcmp(buf,OK) == 0)
					estado = SENDING_DATA;
				else if((strcmp(buf,BYE) == 0) || (strcmp(buf,QUIT) == 0))
					estado = EXIT;
				else
					estado = WAITING_COMMAND;
				break;
			case SENDING_DATA:
				Rio_writen(clientfd, (void *) data, S_DATA);
				estado = WAITING_DATA;
				break;
			case WAITING_DATA:
				printf("Recibiendo trama de %d puntos\n",N);
				n = Rio_readn(clientfd, (void *) data, S_DATA);
				if (n == S_DATA)
					estado = DISPLAY_DATA;
				else
					estado = EXIT;
				break;
			case DISPLAY_DATA:
				print_vector("Resultado FFT:",data,N);
				estado = WAITING_COMMAND;
				break;
			default:
				estado = EXIT;
		}
	}

	Close(clientfd);
	exit(0);
}
